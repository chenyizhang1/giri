#giri设计与实现
---

##一.动态程序切片概念

程序切片是要得出在一个程序中所有直接或间接影响一个变量值或指令值的所有语句。静态程序切片得出所有语句是通过提取任意执行指定变量值的语句，而动态程序切片则是通过给定一个确实在程序中执行的输入，得出真实影响一个变量或指令的所有语句。以下是一个C程序的例子。给出10作为程序的输入，影响返回值的源码行号是：13，20，27,40,42,44,47.行号13、20被包含是由于控制依赖，而行号27,40,42,44,47被包含是因为数据依赖。注意数字15，22应该被包含在静态切片里，而它们不会包含在动态切片中，这是因为在给定10作为输入的时候，它们事实上没有影响到返回值。

	01:  #include <math.h>  
	02:  #include <stdio.h>  
	03:  #include <stdlib.h>  
	04:    
	05:  typedef struct result  
	06:  {  
	07:    int result;  
	08:    int count;  
	09:  } result_t;  
	10:    
	11:  void calc(int x, result_t *y, result_t *z)  
	12:  {  
	13:    if (x < 0)  
	14:    {  
	15:      y->result = sqrt(x);  
	16:      y->count++;  
	17:      z->result = pow(2, x);  
	18:      z->count++;  
	19:    } else {  
	20:      if (x == 0)  
	21:      {  
	22:        y->result = sqrt(x * 2);  
	23:        y->count++;  
	24:        z->result = pow(3, x);  
	25:        z->count++;  
	26:      } else {  
	27:        y->result = sqrt(x * 3);  
	28:        y->count++;  
	29:        z->result = pow(4, x);  
	30:        z->count++;  
	31:      }  
	32:    }  
	33:  }  
	34:    
	35:  int main(int argc, char *argv[])  
	36:  {  
	37:    int x, ret;  
	38:    result_t y = {0, 0}, z = {0, 0};  
	39:    
	40:    x = atoi(argv[1]);  
	41:    
	42:    calc(x, &y, &z);  
	43:      
	44:    ret = printf("%d\n", y.result);  
	45:    printf("%d\n", z.result);  
	46:    
	47:    return ret;  
	48:  }  

#二.giri的基本设计


